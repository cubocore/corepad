/*
    *
    * This file is a part of CorePad.
    * A document editor for C Suite.
    * Copyright 2019 CuboCore Group
    *

    *
    * This program is free software; you can redistribute it and/or modify
    * it under the terms of the GNU General Public License as published by
    * the Free Software Foundation; either version 3 of the License, or
    * (at your option) any later version.
    *

    *
    * This program is distributed in the hope that it will be useful,
    * but WITHOUT ANY WARRANTY; without even the implied warranty of
    * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    * GNU General Public License for more details.
    *

    *
    * You should have received a copy of the GNU General Public License
    * along with this program; if not, write to the Free Software
    * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
    * MA 02110-1301, USA.
    *
*/

#pragma once

#include <QObject>

class QSettings;

class CColorScheme {

public:
	CColorScheme(QString schemeFile = nullptr);

	QString getAttributeColor() const;
	QString getBackgroundColor() const;
	QString getCaretColor() const;
	QString getClassColor() const;
	QString getCommentColor() const;
	QString getCommentDocColor() const;
	QString getCurrentLineColor() const;
	QString getDataTypeColor() const;
	QString getErrorColor() const;
	QString getForegroundColor() const;
	QString getFunctionColor() const;
	QString getKeyword1Color() const;
	QString getKeyword2Color() const;
	QString getNumberColor() const;
	QString getPreprocessorColor() const;
	QString getSelectionColor() const;
	QString getTagColor() const;
	QString getTextColor() const;
	QString getValueColor() const;
	QString getColor(const QString key) const;

private:
	QHash<QString, QString> values;
	void loadSchemeFile(QString schemeFile);

};
