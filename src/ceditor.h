/*
    *
    * This file is a part of CorePad.
    * A document editor for C Suite.
    * Copyright 2019 CuboCore Group
    *

    *
    * This program is free software; you can redistribute it and/or modify
    * it under the terms of the GNU General Public License as published by
    * the Free Software Foundation; either version 3 of the License, or
    * (at your option) any later version.
    *

    *
    * This program is distributed in the hope that it will be useful,
    * but WITHOUT ANY WARRANTY; without even the implied warranty of
    * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    * GNU General Public License for more details.
    *

    *
    * You should have received a copy of the GNU General Public License
    * along with this program; if not, write to the Free Software
    * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
    * MA 02110-1301, USA.
    *
*/

#pragma once

#include <QPlainTextEdit>
#include <QDateTime>

#include "settings.h"


class CSyntaxHighlighter;
class CColorScheme;

class CEditor : public QPlainTextEdit {
	Q_OBJECT

public:
	explicit CEditor(QString fPath, CColorScheme *colorScheme, QWidget *parent = nullptr);
	~CEditor();

	void updateColorScheme(CColorScheme *colorScheme);

	void loadFile(const QString &);
	bool saveFile(bool newFile = false);
	void reloadFile();

	bool isCopyAvailable();
	QString workFilePath() const;
	bool hasChange();
	bool canClose();

	QDateTime getModifiedTime();

	int lineNumberAreaWidth();
	QWidget *lineNumberArea() const;

	void lineNumberAreaPaintEvent(QPaintEvent *);

protected:
	void wheelEvent(QWheelEvent *event) override;
	void resizeEvent(QResizeEvent *) override;

private slots:
	void textChangedUpdate();
	void highlightCurrentLine();
	void updateLineNumberAreaWidth();
	void updateLineNumberArea(const QRect &, int);

private:
	CSyntaxHighlighter *chighlighter = nullptr;
	CColorScheme *scheme;
	QWidget *m_lineNumberArea;
	bool m_copyAvailable;
    QString m_filePath;
    settings            *smi;
	QColor currentLineColor;
	QDateTime m_modifiedTime;

	void toggleOverwrite();
	void setCopyAvailable(bool);
	QString getSyntaxFile(QString suffix);
	void loadSyntax();

signals:
	void unsavedChanges(QString);
	void fileLoaded(QString);
};


class LineNumberArea : public QWidget {

public:
	LineNumberArea(CEditor *editor) : QWidget(editor) {
		m_ceditor = editor;
	}

	QSize sizeHint() const override {
		return QSize(m_ceditor->lineNumberAreaWidth(), 0);
	}

protected:
	void paintEvent(QPaintEvent *event) override {
		m_ceditor->lineNumberAreaPaintEvent(event);
	}

private:
	CEditor *m_ceditor;

};
